import logo from "./logo.svg";
import "./App.css";
import { useSelector, useDispatch } from "react-redux";
import { decrement, increment } from "./features/counter/counterSlice";
import { useEffect } from "react";

const ComponentUbahValue = () => {
  const dispatch = useDispatch();
  
  return (
    <div>
      <button
              aria-label="Increment value"
              onClick={() => dispatch(increment())}
            >
              Increment
      </button>
      <button
              aria-label="Increment value"
              onClick={() => dispatch(increment())}
            >
              Increment
      </button>
    </div>
  )
}
const ComponentLain = () => {
  const count = useSelector((state) => state.counter.value);
  useEffect(() => {
    console.log('if count changing')
    
  }, [count])
  return (
    <div>
      Hello this is other counter {count}
      <br /> <br />
      <ComponentUbahValue />
    </div>
  )
}
function App() {
  const count = useSelector((state) => state.counter.value);
  const dispatch = useDispatch();

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <div>
          <div>
            <button
              aria-label="Increment value"
              onClick={() => dispatch(increment())}
            >
              Increment
            </button>
            <span>{count}</span>
            <button
              aria-label="Decrement value"
              onClick={() => dispatch(decrement())}
            >
              Decrement
            </button>
          </div>
        </div>
        <ComponentLain />
      </header>
      
    </div>
  );
}

export default App;
